read -p "Introduzca una cantidad válida de litros consumidos de agua, por favor: " litros

p1=0.40
p2=0.20
p3=0.10
ps50=20
ps150=30
acum=0
agua1=50
agua2=150
aguamax=`expr $agua1 + $agua2`

while [ $litros -le 0 ]; do
    echo " "
    read -p "Introduzca una cantidad válida de litros consumidos, por favor: " litros
    echo " "
done

if [ $litros -le $agua1 ]; then
    echo " "
    echo "El coste total en euros es de: "
    echo "scale=2; $litros * $p1" | bc

elif [ $litros -gt $agua1 ] && [ $litros -le $agua2 ]; then
    acum=`expr $litros - $agua1`
    echo " "
    echo "El coste total en euros es de: "
    echo "sclae=2; $acum * $p2 + $ps50" 

else
    acum=`expr $litros - $aguamax`
    echo " "
    echo "El coste total en euros es de: "
    echo "scale=2; $acum * $p3 + $ps50 + $ps150" | bc
fi
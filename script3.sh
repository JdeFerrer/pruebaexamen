read -p "Introduzca la nota del alumno, por favor: " nota

valormin=0
valormax=10

while [ $nota -lt $valormin ] || [ $nota -gt $valormax ]; do
    read -p "Le hemos indicado que introduzca la nota del alumno, pero esta debe de ser una nota válida (de 0 a 10): " nota
done

if [ $nota -ge $valormin ] && [ $nota -lt 5 ]; then
    echo "Has suspendido por haber sacado un $nota, suerte la próxima"

else if [ $nota -ge 5 ] && [ $nota -lt 6 ]; then
    echo "Has aprobado al haber sacado un $nota, a la próxima esfuérzate más"

else if [ $nota -ge 6 ] && [ $nota -lt 7 ]; then
    echo "Has obtenido un bien al tener un $nota"
else if [ $nota -ge 7 ] && [ $nota -lt 10 ]; then
    echo "Has obtenido un notable, al obtener un $nota, muy bien!"
else
    echo "Has obtenido un sobresaliente, eres un crack, catacrack!!!!!"
fi
fi
fi
fi
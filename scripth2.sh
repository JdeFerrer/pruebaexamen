read -p "Introduzca un número mayor de 0, por favor: " n

resto=0

while [ 1 -ge $n ]; do
    read -p "Por favor, le hemos indicado que introduzca un número mayor de 0, hágalo: " n
done

resto=`expr $n % 2`

if [ $resto -eq 0 ]; then
    echo "Has introducido el número $n y, por tanto, es un número par"
else
    echo "HAs introducido el número $n y, por tanto, es un número impar"
fi
read -p "Introduzca un valor mayor o igual a 0, pero si introduce el 0, el script se dará por terminado: " num

valor=0
contador=0
acumulador=0
uno=1
promedio=0
while [ $num -ne $valor ]; do

    if [ $num -gt $valor ]; then

	acumulador=`expr $acumulador + $num`
	contador=`expr $contador + $uno`
	echo " "
	read -p "Introduzca un número, por favor, si quiere acabar, introduzca un 0 :" num
	echo " "
    else
       	echo " "
	read -p "Le hemos indicado que debe de introducir un número superior o igual a 0, hágalo: " num
	echo " "
   fi
done

promedio=`expr $acumulador / $contador`

echo "Suma total de los valores introducidos: $acumulador"
echo " "
echo "Valor promedio de los valores introducidos: $promedio"